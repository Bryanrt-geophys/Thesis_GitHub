
* TISC INPUTFILE BASIC TEST *

Parameters at 'flex.PRM'.

### TISC parameters file ###

#TISC VERSION FOR THIS FILE:
#version	TISC_2017-05-19
version		tisc v4.0

xmin             0e3   Western model limit coordinate [m]     
xmax            +500e3   Eastern model limit coordinate [m]
ymin            0e3   Southern model limit coordinate [m]
ymax            +500e3   Northern model limit coordinate [m]
zmin    	-5000
zmax    	8000

#Gridding:
Nx              251     E-W direction grid points
Ny              251     N-S direction grid points

#Initial relief:
zini            700       Initial height [m] of the plate over the sea level (added to the initial topography in '*.H0').
random_topo     100     Noise in initial relief [m]


#DENSITIES [kg/m3]:
densmantle      3250    Density beneath the plate, at the compensation level: mantle or asthenosphere
denscrust       2850    Mean density of crust
densinfill      2800    Infill density used to fill under topographic loads.
denssedim       2200    Sediment density
densenv         0       Enviromental material density

sed_porosity 0.3		Mean porosity of sediment at deposition (seafloor). For
				density purposes, pores are assumed to be filled by water. The
				bulk density is fixed through 'denssedim', this porosity is used
				just to convert eroded/deposited volumes into bulk rock mass.
compact_depth 1000	Depth at which sediment porosity is reduced by a factor e,
				due to compaction [m]. 0 (zero) means no compaction.

#TIME VARIABLES [My] (time goes from negative to positive: Timeini<Timefinal):
Timeini         0       Initial time
Timefinal       22       Final time
dt              1      Time interval
dt_eros         .01     Approximate dt for surface processes
dtmemounit      5       Time between horizon record (0 means no horizons apart from those specified in '*.REC' file) and deflection evolution.

dt_record	.25	Time lag between records of sedimentary horizons (time lines)
				and deflection evolution (written in '*.xyzt'). If =0, will be
				recorded only at the times of *.UNIT files, and at those times
				specified in the '*.REC' file. (>0)

#FLEXURE:
isost_model	1	Isostasy mechanism: 
			#0: No isostasy.
			#1: Pure elastic thin plate.
			#2: Viscoelastic thin plate.
Te		25e3	Default EET [m] when no *.EET file is given. Te=0 => local
				isostasy. Usual values are between 0 and 150 km.
tau		1	Relaxation time in Myr (used only for the viscoelastic plate model:
				isost_model=2). tau=viscosity*(2*(1+nu))/E. 1 Myr implies a 
				lithospheric viscosity of 1e24 Pa s.
boundary_conds	5 	Boundary conditions for flexure at N, S, E, W (4 
			characters):
			#0: means fixed boundary (zero deflection).
			#5: means free boundary (null 1st and second derivates, 
				i.e., null slope and moment).
			#6: fixes local isostatic deflection at boundary.
water_load	1	To take into account sea+lake load and allow for temporal sea
				level variations read from '*.SLV'.
topoest		0	If 1 then the final topography (instead of load thickness)
				will be read from *.UNIT files. That topography is vertically
				fixed, filling the deflection with densinfill material.
#Tectonic horizontal forces [Pa*m=N/m]:
Px		0	Horizontal tectonic force in x (compressive => >0)
Py		0	Horizontal tectonic force in y
Pxy		0	Horizontal tectonic shear force xy

#CLIMATE AND SURFACE WATER FLOW (params. used only if hydro_model=1):
hydro_model	1
rain		600	[l/m2/yr]=[mm/yr]=[1.58e-8 m3/m2/s].
Krain		00
CXrain		0	
CYrain		0	
evaporation	1500	Evaporation rate at lakes [l/m2/yr]=[mm/yr]. evaporation>runoff
				can significantly slow down the hydrological calculations. 
			If hydro_model=1,2: laterally constant evaporation.
			If hydro_model=3: evaporation caused by dry air and 0 wind speed. E
				is then calculated as: E=evaporation * (1+beta*wind) *
				(Wmax-Wcol)/Wmax
lost_rate	2       Water evapotranspirated per river unit length [%/km]

#EROSION/SEDIMENTATION PARAMETERS (params. used only if erosed_model=1):
erosed_model	8
				
Keroseol	0	Continental background erosion rate (ratio per My, [m/(m�My)]).
Ksedim  	1e2	Additional sedimentation rate below sea level [m/My].
Kerosdif   	.2	
critical_slope	0	Maximum surface slope (slope triggering landsliding, 
				[m/m]), (0 means no landsliding). 
K_river_cap	500
l_fluv_eros	120e3
l_fluv_eros_sed	60e3	Length scale of fluvial erosion	[m] (for sediments)
l_fluv_sedim	5e3	Sedimentation decay distance [m] (fluvial aggradation 
				or delta sedimentation). For erosed_model=3, this is only
				relevant for delta sedimentation at the sea and lakes. Larger
				values imply wider deltas and a more gradual transition from
				river incision to river aggradation.
deform_sed	1	0 means that sediment units will remain steady (velocity =0); 1
				means that sediment will be automatically deformed by the
				motion of Blocks.
erodibility	1e-07	
erodibility_sed	1e-06	Same as 'erodibility' but for sediments. 
eros_bound_cond	0000	Boundary Conditions for surface processes at the N, S,
				E, W boundaries of the model (4 characters):
			#0: means river has 0 equilibrium capacity (q_eq in 
				Garcia-Castellanos,2002) at boundary.
			#1: means that the river keeps constant slope across the boundary.
			#2: means zero river erosion or sedimentation.
			#3: means half capacity.
			#c: as 1 but under sea-level areas will not be connected to ocean
				at that boundary. 
K_ice_eros	0
switch_sea	0
#INPUT/OUTPUT:
verbose_level	1	Specify the level of runtime prints in stdout.
			#0: Errors and basic information are printed. 
			#1: Statistics are also printed (default). 
			#2: Info: Prints additional information. 
			#3: Warnings: As '1' but further i/o info is provided.
			#4: Debug: Prints debugging info to locate errors.
			#5: DebugPlus: Prints more debugging information.
switch_files	1	For extra file output: see files *.bas *.all *.lakes in
				doc/tisc.info.txt.
mode_interp	4	Default interpolation mode for input files. See
				doc/template.UNIT.
switch_ps	1	For postscript graphic output (uses GMT4 shell scripts).


