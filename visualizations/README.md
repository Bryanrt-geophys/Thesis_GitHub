# The Plots
To streamline computation time and for ease of visualization, all plots were made from three-dimensional data. A transect is prescribed from an input file (<project_name>.prfl) when designing TISC models. All data points for bedrock and sediment units are taken from the four-dimensional model along this transect, thus reducing these visualizations one dimension. Greater detail and alternative methods can be found in the documentation within the TISCViewR package.  

### basin_depth
Basin depth is measured from sea level to maximum point of depth within the basin, in regard to the top of the basement, at a given timestep. As such, basin depth does not correspond with a location within the model. These figures are ideal for compare the rates of subsidence different basin building mechanisms create.

### basin_length
Basin length is a measure of surficial expression of wavelength. Measurements are taken from the first point that the bedrock goes below sea level in front of the thrust system, until bedrock rises back above sea level along the basinward side of the forebulge development.  

### basin_plotly
Using the Plotly package in R, basin_plotly is populated with interactive 3D surface plots depicting the geometry of the basin development, as measured along a transect, through time. This allows the modeler to readily visualize correlations in basin geometries resultant from unique surficial or flexural conditions.

### basin_plots
Three subdomains are contained within basin_plots and are visualizations of the data in steps along the way to create the Plotly surfaces, the symmetry plots contained in basin_symmetry, basin_depth, and basin_length. 

### basin_symmetry
Basin symmetry is measured by taking a Riemann sum of two halves of the basin, split at the median, per time step. This results in two areas, the hinterlandward area and the thrustward area, being available for each timestep within a model. These areas are then used to determine a ratio which can be viewed as a pseudo measurement for skewness. A perfectly symmetrical basin has a ratio value of one. A value above one represents a basin skewed towards being deeper near the thrust front, while a value below one represents a basin deeper near the hinterland. These plots allow the modeler to visualize the dynamic and unique rebound patterns from different flexural and surficial conditions. 

### geohistory_curves
Decompaction and backstripping techniques are applied to calculate the total subsidence, tectonic subsidence, and basin water depth within each model in regard to a pseudo well location. The well location is predefined by TISCViewR at the point of greatest basin depth from the final timestep of a model and then that point is used to gather and evaluate the data from each timestep within the model. As the model preserves data from each timestep, these curves can be compared to curves created from real world data to determine differences in case study model designs and, theoretically, discrepancies real world geohistory curve calculations introduce under assumptions made to interpolate back through time.

